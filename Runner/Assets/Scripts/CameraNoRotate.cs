using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraNoRotate : MonoBehaviour
{
    
    private float smoothTime = 0.01f; // the time it takes for the camera to catch up to the player's position
    private Vector3 offset; // the distance between the camera and the player

    private Vector3 velocity = Vector3.zero; // used to smooth the camera's movement


    private Quaternion my_rotation;
    [SerializeField]
    private GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        offset = this.transform.position - target.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // smooth the camera's position by using SmoothDamp
        transform.position = Vector3.SmoothDamp(transform.position, target.transform.position + offset, ref velocity, smoothTime);
       // transform.rotation = Vector3.SmoothDamp(transform.rotation, target.transform.rotation, ref velocity, smoothTime);
    }

}
