using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    // A list of items that represent the items in the player's inventory
    private List<IInteractible> weapons = new List<IInteractible>();
    // The maximum number of items 
    private int maxItems = 5;
    // The item that the player is currently holding in their hand
    private IInteractible itemInHand;

    // Picks up an item and adds it to the inventory
    public void PickUp(IInteractible item)
    {
        // Only pick up the item if the inventory isn't full and only add to the inventory if it's not n2o/heart/gas
        if (weapons.Count < maxItems)
        {
            weapons.Add(item);
            itemInHand = item;
            //Notify item that it has been picked up
            item.OnPickUp();
        }
    }

    // Drops an item and removes it from the inventory
    private void Drop(IInteractible item)
    {
        weapons.Remove(item);
        itemInHand = null;
        //Notify item that it has been dropped
        item.OnDrop();
    }

    // Uses the item currently in the player's hand
    private void Use()
    {
        // Only use the item if the player is currently holding one
        if (itemInHand != null)
        {
            itemInHand.OnUse();
        }
    }
}