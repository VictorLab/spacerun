using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PickUpItems : MonoBehaviour
{
    // Start is called before the first frame update
    private float Gas;
    private float MaxGas;

    [SerializeField]
    private GameObject Raygun;
    private float health;
    [SerializeField]
    private Health healthScript;
    [SerializeField]
    private PlayerMovement playerMovementScript;
    [SerializeField]
    private GameObject ProjectileGun;
    private ProjectileGun projectileGunScript;

    [SerializeField]
    private GameObject GameOver;
    void Start()
    {
        healthScript = this.gameObject.GetComponent<Health>();
        playerMovementScript = this.gameObject.GetComponent<PlayerMovement>();
        projectileGunScript = ProjectileGun.gameObject.GetComponent<ProjectileGun>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Gas")
        {
            playerMovementScript.AddFuel(25);
            Destroy(other.gameObject);
        }

       else if (other.tag == "TripleGas")
        {
            playerMovementScript.AddFuel(75);
            Destroy(other.gameObject);
        }

        // food
       else if (other.tag == "Food")
        {
            Food food = other.GetComponent<Food>();
            healthScript.AddHealth((int)food.fhealth);
            Destroy(other.gameObject);
        }


        // guns
     else   if (other.tag == "Raygun")
        {

            Raygun.SetActive(true);
            Destroy(other.gameObject);

        }

        else if (other.tag == "Ammo")
        {
            Debug.Log("AMMOHIT");
            Debug.Log("AMMOHIT" + projectileGunScript);
            projectileGunScript.AddAmmo(30);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.name.Contains("killerBug"))
        {
            healthScript.RemoveHealth(1);
           // Destroy(other.gameObject);
        }

        else if (other.tag == "Death")
        {

            GameOver.SetActive(true);
            this.GetComponent<PlayerMovement>().anim.enabled = false;
            this.GetComponent<PlayerMovement>().enabled = false;

        }


        else if (other.tag == "finish")
        {
            Debug.Log("finish");
            if (SceneManager.GetActiveScene().name == "Level1")
            {

                SceneManager.LoadScene("Level2");
            }
            else if (SceneManager.GetActiveScene().name == "Level2")
            {

                SceneManager.LoadScene("Level3");
            }
           else if (SceneManager.GetActiveScene().name == "Level3")
            {

                SceneManager.LoadScene("Level4");
            }
           else if (SceneManager.GetActiveScene().name == "Level4")
            {

                SceneManager.LoadScene("Level4");
            }

        }

       
    }
    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Enemy" && other.gameObject.name.Contains("killerBug"))
        {
            healthScript.RemoveHealth(1);
            //Destroy(other.gameObject);
        }
    }
}
