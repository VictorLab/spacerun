using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int totalHealth;
    [SerializeField]
    private Slider healthSlider;
    private int currentHealth;
    private TextMeshPro deathText;
    [SerializeField]
    private AudioClip hitSound;
    private AudioSource audioSource;
    [SerializeField]
    private GameObject gameOver;
    public CartoonPostProcessingEffect cartoonEffect; // Make sure to drag and drop the CartoonPostProcessingEffect component in the editor to this variable
    private PlayerMovement playerMovement;

    // Define an event for when the player takes damage
    public delegate void PlayerDamagedEventHandler();
    public static event PlayerDamagedEventHandler OnPlayerDamaged;

    void Start()
    {
      

  //  healthSlider.gameObject.SetActive(false); // Hide the health bar at the start

        healthSlider.maxValue = totalHealth;
        currentHealth = totalHealth;
        healthSlider.value = currentHealth;
        void Start()
        {
            // ... existing code ...

            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }
        }

    }



    // Update is called once per frame
    void Update()
    {
        
    }
    public void AddHealth(int healthToAdd)
    {

        if (currentHealth <= totalHealth)
        {
            if (currentHealth + healthToAdd >= totalHealth)
            {
                totalHealth = (int)healthSlider.maxValue;
                healthSlider.value = currentHealth;
            }
            else
            {


                Debug.Log("currentHealth" + currentHealth + "tot" + totalHealth);
                currentHealth += healthToAdd;
                healthSlider.value = currentHealth;
            }
        }
    }
    public void RemoveHealth(int healthToRemove)
    {
        if (currentHealth > 0)
        {
            currentHealth -= healthToRemove;
            healthSlider.value = currentHealth;
            if (currentHealth <= 0)
            {
                gameOver.SetActive(true);
                this.GetComponent<PlayerMovement>().anim.enabled = false;
                this.GetComponent<PlayerMovement>().enabled = false;
            }
            else
            {
                // Apply the damage effects without resetting the current effects
                cartoonEffect.ApplyDamageEffects();
            }

            // Play the hit sound
            if (hitSound != null && audioSource != null)
            {
                audioSource.PlayOneShot(hitSound);
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemyBullets")
        {
            currentHealth--;
            healthSlider.value = currentHealth;
            // Trigger the event when the player takes damage
            OnPlayerDamaged?.Invoke();
        }
    }
}
