using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
public class PlayerMovement : MonoBehaviour
{

    // Movement variables
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private float speedForward;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private float gravity = -30;
    [SerializeField]
    private float rotSpeed = 0.6f; // in seconds
    [SerializeField]
    private float maximumGravityVelocity = 25;
    private float maximumMove = 0.85f;
    private float maximumMovePressed = 1.3f;
    [SerializeField]
    private float ActivateJetpacktimeAfterJumping = 0.4f; // this should be tweaked to increase realism
    // Add this line to reference the CartoonPostProcessingEffect script
    public CartoonPostProcessingEffect cartoonEffect;

    //inputs
    private float inputHorizontal;
    private float inputVertical;
    private float inputHorizontalRaw;  // either 0 or 1
    private float inputVerticalRaw;
    float lastInputVertical;  // last input before InputRaw = 0
    float lastInputHorizontal;

    // Necessary components for animation and movement
    [SerializeField]
    private GameObject Astro;
    public Animator anim;
    private CharacterController _controller;
    private Camera MainCamera;
    [SerializeField]
    private GameObject legs;
    /*
    //jumping stuff
    private bool isJumping1s;
    private bool isJumping;
    */

    // variable for rotating while grounded
    public bool rotating;

    //List of objects for the jetpack particles
    [SerializeField]
    private GameObject fireJetDownGO;
    [SerializeField]
    private GameObject fireJetUpGO;
    [SerializeField]
    private GameObject fireJetUpperRightGO;
    [SerializeField]
    private GameObject fireJetLowerRightGO;
    [SerializeField]
    private GameObject fireJetUpperLeftGO;
    [SerializeField]
    private GameObject fireJetLowerLeftGO;

    private bool thrusters = false;
    private ParticleSystem fireJetDownPS;
    private ParticleSystem fireJetUp;
    private ParticleSystem fireJetUpperRight;
    private ParticleSystem fireJetLowerRight;
    private ParticleSystem fireJetUpperLeft;
    private ParticleSystem fireJetLowerLeft;

    // Vectors for rotating the player on the right wall
    Vector3 leftRotateVector = new Vector3(0, 0, -90);
    Vector3 rightRotateVector = new Vector3(0, 0, 90);
    Vector3 bottomRotateVector = new Vector3(0, 0, 0);
    Vector3 upRotateVector = new Vector3(0, 0, 180);
    Vector3 move = Vector3.zero;
    Vector3 startRotPos;
    Quaternion targetRotation;
    //


    //jetpackl slider
    [SerializeField]
    private Slider FuelSlider;

    private float totalFuel = 90;
    private float currentFuel = 0;

    private float fuel = 100f; // initial fuel value
    private float decreaseRate = 0.3f; // rate at which fuel decreases
    private float firstPressDecrease = 2.5f; // fuel decrease on first press or direction change
    private float nextDecreaseTime; // time at which fuel will next decrease
    private float previousHorizontalInput; // store previous horizontal input
    private float previousVerticalInput;

    private float horizontalInput;
    private float verticalInput;
    // Enum to represent the different states of the player: (Flying happens only after using the jetpack while jumping)
    private enum PlayerState
    {
        Grounded,
        Jumping,
        Flying,
        Grappling
    }
    private PlayerState currentState;

    // Enum for the player's rotation
    private enum Rotation
    {
        Down,
        Right,
        Left,
        Up
    }
    private Rotation currentRot;

    void Start()
    {
        // Get references to the necessary components
        _controller = GetComponent<CharacterController>();
        anim = Astro.GetComponent<Animator>();

        // Get particleSystems
        fireJetDownPS = fireJetDownGO.GetComponent<ParticleSystem>();
        fireJetUp = fireJetUpGO.GetComponent<ParticleSystem>();
        fireJetUpperRight = fireJetUpperRightGO.GetComponent<ParticleSystem>();
        fireJetLowerRight = fireJetLowerRightGO.GetComponent<ParticleSystem>();
        fireJetUpperLeft = fireJetUpperLeftGO.GetComponent<ParticleSystem>();
        fireJetLowerLeft = fireJetLowerLeftGO.GetComponent<ParticleSystem>();


        // Set the initial states
        currentState = PlayerState.Jumping;
        currentRot = Rotation.Down;
    }

    // Input for running to the right & left on the ground, and also up & down when using the jetpack.
    void Update()
    {
        inputHorizontal = Input.GetAxis("Horizontal");
        inputVertical = Input.GetAxis("Vertical");
        inputHorizontalRaw = Input.GetAxisRaw("Horizontal");
        inputVerticalRaw = Input.GetAxisRaw("Vertical");

        //if: jump button pressed & grounded & not jumping yet (2nd option for holdingh down space)
        if (!rotating && Input.GetButtonDown("Jump") && currentState == PlayerState.Grounded || !rotating && (Input.GetKey(KeyCode.Space) && currentState == PlayerState.Grounded))
        {
            StartCoroutine(StartJumping(jumpForce));

        }

        if (!rotating && currentState == PlayerState.Flying && thrusters && (inputHorizontal != 0 || inputVertical != 0))
        {

            JetpackThrusterParticles();
            if (anim.GetBool("Grounded") == true) {
                anim.SetBool("Grounded", false);
            }
        }

    }

    /*The OnCollisionEnter() function is called when the character collides with another object.
     * When the other collider has a "Ground" tag this sets the character's PlayerState to Grounded.
     * If it does, the function calls the CheckDirection() function and passes in the normal of the point of contact between the character and the ground as an argument.
     */
    void OnCollisionEnter(Collision c)
    {

        if (c.gameObject.CompareTag("Ground"))
        {
            //  rotating = false;
            lastInputVertical = 0;
            lastInputHorizontal = 0;
            Debug.Log("entering ground and jumping");
            currentState = PlayerState.Grounded;
            anim.SetBool("Grounded", true);
            StopJetPack();
            Vector3 direction = c.GetContact(0).normal;
            CheckDirection(direction);


            foreach (var item in c.contacts)
            {
                Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            }

        }

        Debug.Log("collwith"+ c.gameObject.name);

    }


    // Use oncollisionStay to recheck if still grounded to prevent oncollision exit
    void OnCollisionStay(Collision c)
    {

        if (c.gameObject.CompareTag("Ground"))
        {
            //  rotating = false;
            //  Debug.Log("entering ground and jumping");
            currentState = PlayerState.Grounded;
            anim.SetBool("Grounded", true);
           // StopJetPack();
        //    Vector3 direction = c.GetContact(0).normal;
        //    CheckDirection(direction);
        }
    }

    /* when the playerr leaves the ground object, reset velocity towards the ground to make the player realistically slide through space with gravity applied.

    */
    void OnCollisionExit(Collision c)
    {

        if (c.gameObject.CompareTag("Ground") && currentState == PlayerState.Grounded)
        {
            currentState = PlayerState.Flying;
            anim.SetBool("Grounded", false);
            if (currentRot == Rotation.Down)
            {
                velocity.y = 0;
                print("started jumpup");
            }
            else if (currentRot == Rotation.Left)
            {
                velocity.x = 0;
                print("started jumpfromleft");
            }
            else if (currentRot == Rotation.Right)
            {
                velocity.x = -0;
                print("started jumpfromright");
            }
            else if (currentRot == Rotation.Up)
            {
                velocity.y = -0;
                print("started jumpfromup");
            }
            StartCoroutine(StartJumping(0));
            //  currentState = PlayerState.Jumping;

            thrusters = true;

        }
    }



    /* after OnCollision and CheckDirections > Rotate towards the right rotation using Slerp(). 
     * This coroutine also sets the "Turn" bool in the character animator to true while the rotation is happening
    */

    private IEnumerator AnimateRotationTowards(Quaternion transformRot, Quaternion targetRot, float dur)
    {
        // compare quaternion transformRot and targetRotation, based on that use left or right lower booster to turn
        //  transformRot = this.transform.rotation;
        startRotPos = transform.position;

        rotating = true;
        //RemoveFuel(0.1);
        // reset vel/movement
        velocity.x = 0;
        velocity.y = 0;
        move.x = 0;
        move.y = 0;
        move.z = 0;
        velocity.z = 0;
        anim.SetBool("Turn", true);
        JetPackTurn(transformRot, targetRot);
        float t = 0f;
        Debug.Log("TGROT" + targetRot);

      /*  Quaternion currentRotation = targetRot;
        Vector3 eulerAngles = currentRotation.eulerAngles;
        float roundedZ = Mathf.Round(eulerAngles.z / 90) * 90;
        eulerAngles.z = roundedZ;
        Quaternion roundedRotation = Quaternion.Euler(eulerAngles);
        if (currentRotation != roundedRotation)
        {
            Debug.Log("TGROT" + targetRot);
            targetRot = roundedRotation;
            Debug.Log("TGROT" + targetRot);
        }*/

        while (t < dur)
        {
            this.transform.rotation = Quaternion.Slerp(transformRot, targetRot, t / dur);

            t += Time.deltaTime;
            yield return null;
        }

        float currentRotation = transform.rotation.eulerAngles.z;
        float roundedRotation = Mathf.Round(currentRotation / 90) * 90;
        if (currentRotation != roundedRotation)
        {
            transform.rotation = Quaternion.Euler(0, 0, roundedRotation);
        }

        //   rotating = false;
        anim.SetBool("Grounded", true);
        anim.SetBool("Turn", false);
        yield return new WaitForSeconds(0.1f);
        rotating = false;
    }

    /*The CheckDirection() function checks the direction of the normal passed to it, and if the direction is left, right, down, or up, 
     * it starts a coroutine called AnimateRotationTowards() and passes in the current rotation of the character, 
     * the target rotation for the character to face the direction, and a duration for the rotation to take.
     */
    void CheckDirection(Vector3 direction)
    {


        if (currentRot != Rotation.Left && direction.x > 0.9)
        {
            currentRot = Rotation.Left;
            //     velocity.y = 0;
            StartCoroutine(AnimateRotationTowards(this.transform.rotation, Quaternion.Euler(leftRotateVector), rotSpeed));
        }

        else if (currentRot != Rotation.Right && direction.x < -0.9)
        {
            Debug.Log("right");
            currentRot = Rotation.Right;
            //     velocity.y = 0;
            StartCoroutine(AnimateRotationTowards(this.transform.rotation, Quaternion.Euler(rightRotateVector), rotSpeed));
        }


        else if (currentRot != Rotation.Down && direction.y > 0.9)
        {
            Debug.Log("down");
            currentRot = Rotation.Down;
            //      velocity.x = 0;
            StartCoroutine(AnimateRotationTowards(this.transform.rotation, Quaternion.Euler(bottomRotateVector), rotSpeed));
        }

        else if (currentRot != Rotation.Up && direction.y < -0.9)
        {
            Debug.Log("up");
            currentRot = Rotation.Up;
            //    velocity.x = 0;
            StartCoroutine(AnimateRotationTowards(this.transform.rotation, Quaternion.Euler(upRotateVector), rotSpeed));
        }



    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.8f);
    }





    //checks rotation and adds jumpforce to the correct xy velocity
    IEnumerator StartJumping(float jumpForce)
    {

        lastInputVertical = inputVertical;
        lastInputHorizontal = inputHorizontal;
        thrusters = false;
        Debug.Log("thrusters1" + thrusters);
        currentState = PlayerState.Jumping;
        if (jumpForce > 1)
        {
            anim.SetTrigger("Jump");
        }
        anim.SetBool("Grounded", false);
        print("started jump");
        //  Debug.Log("rotating" + rotating.ToString());

        if (currentRot == Rotation.Down)
        {
            velocity.y = jumpForce;
        }
        else if (currentRot == Rotation.Left)
        {
            velocity.x = jumpForce;
        }
        else if (currentRot == Rotation.Right)
        {
            velocity.x = -jumpForce;
        }
        else if (currentRot == Rotation.Up)
        {
            velocity.y = -jumpForce;
        }
        Debug.Log("thrusters2" + thrusters);
        //   yield return new WaitForSeconds(0.2f);

        yield return new WaitForSeconds(ActivateJetpacktimeAfterJumping);
        anim.SetBool("Grounded", false);

        currentState = PlayerState.Flying;
        yield return new WaitForSeconds(0.05f); //prevent thrusters showing while awsd pressed when jumping

        // Call the ApplyRandomCombinationEffects method when the player jumps
        if (cartoonEffect != null)
        {
            cartoonEffect.ApplyRandomCombinationEffects();
            Debug.LogWarning("CartoonPostProcessingEffect should work!!!");
        }
        else
        {
            Debug.LogWarning("CartoonPostProcessingEffect reference not set in PlayerMovement!");
        }

        thrusters = true;
        anim.SetBool("Grounded", false);



        Debug.Log("thrusters3" + thrusters);
        // isJumping = false;


    }
    void FixedUpdate()
    {

        if (!rotating)
        {
            //    Debug.Log("rotating?" + rotating);

            //If not rotating and not grounded, or if jumping pressed <1s ago, apply gravity based on rotation
            if (!rotating && currentState == PlayerState.Grounded)
            {
                move = transform.right * inputHorizontal;

            }

            {
                //apply gravity
                if (!rotating && currentState == PlayerState.Jumping || !rotating && currentState == PlayerState.Flying)
                {

                    if (currentRot == Rotation.Down)
                    {
                        velocity.y += gravity * Time.deltaTime;
                    }
                    else if (currentRot == Rotation.Left)
                    {
                        velocity.x += gravity * Time.deltaTime;
                    }
                    else if (currentRot == Rotation.Right)
                    {
                        velocity.x -= gravity * Time.deltaTime;
                    }
                    else if (currentRot == Rotation.Up)
                    {
                        velocity.y -= gravity * Time.deltaTime;
                    }

                }
                if (!rotating)
                {
                    //set maximum gravity/velocity to float through space
                    if (velocity.x < -maximumGravityVelocity)
                    {
                        velocity.x = -maximumGravityVelocity;
                    }
                    if (velocity.y < -maximumGravityVelocity)
                    {
                        velocity.y = -maximumGravityVelocity;
                    }
                    if (velocity.y > maximumGravityVelocity)
                    {
                        velocity.y = maximumGravityVelocity;
                    }
                    if (velocity.x > maximumGravityVelocity)
                    {
                        velocity.x = maximumGravityVelocity;
                    }
                }
                // If not rotating and grounded is true then move on inputhorizontal

                // jetpack stuff
                if (currentFuel > 0)
                {

                    if (currentFuel < 1.4f)
                    {
                        StopJetPack();
                    }
                    if (!rotating && currentState == PlayerState.Flying && (inputHorizontal != 0 || inputVertical != 0))
                    {

                        // Resets gravity pull when thrusting upwards

                        if (currentRot == Rotation.Down && inputVerticalRaw > 0)
                        {
                            velocity.y = 7;
                        }
                        else if (currentRot == Rotation.Left && inputVerticalRaw > 0)
                        {
                            velocity.x = 7;
                        }
                        else if (currentRot == Rotation.Right && inputVerticalRaw > 0)
                        {
                            velocity.x = -7;
                        }
                        else if (currentRot == Rotation.Up && inputVerticalRaw > 0)
                        {
                            velocity.y = -7;
                        }

                        /*
                         if (currentRot == Rotation.Down && inputHorizontalRaw > 0)
                         {
                             move.x = 1;
                         }
                         else if (currentRot == Rotation.Left && inputHorizontalRaw > 0) //
                         {
                             move.y = -1;
                         }
                         else if (currentRot == Rotation.Right && inputHorizontalRaw > 0)
                         {
                             move.y = 1;
                         }
                         else if (currentRot == Rotation.Up && inputHorizontalRaw > 0)
                         {
                             move.x = -1;
                         }

                         if (currentRot == Rotation.Down && inputHorizontalRaw < 0)
                         {
                             move.x = -1;
                         }
                         else if (currentRot == Rotation.Left && inputHorizontalRaw < 0)//
                         {
                             move.y = 1;
                         }
                         else if (currentRot == Rotation.Right && inputHorizontalRaw < 0)
                         {
                            move.y = -1;
                         }
                         else if (currentRot == Rotation.Up && inputHorizontalRaw < 0)
                         {
                             move.x =  1;
                         }*/

                        if (inputVerticalRaw != 0)
                        {
                            move += transform.up * (inputVertical / 2); //+ transform.right * lastInputHorizontal;
                                                                        // lastInputVertical = inputVertical;

                            decreaseFuelVert();

                        }
                        else
                        {
                            previousVerticalInput = 0;
                        }

                        if (inputHorizontalRaw != 0)
                        {
                            move += transform.right * (inputHorizontalRaw / 3); //+ transform.right * lastInputHorizontal;
                                                                                // lastInputVertical = inputVertical;
                            decreaseFuelHori();                                         // decrease fuel if it's time

                        }
                        else
                        {
                            previousHorizontalInput = 0;
                        }


                        if (inputHorizontalRaw == 0 && inputVerticalRaw == 0)
                        {
                            //set maximummove to float through space when  not using jetpack
                            if (move.x < -maximumMove)
                            {
                                move.x = -maximumMove;
                            }
                            if (move.y < -maximumMove)
                            {
                                move.y = -maximumMove;
                            }
                            if (move.y > maximumMove)
                            {
                                move.y = maximumMove;
                            }
                            if (move.x > maximumMove)
                            {
                                move.x = maximumMove;
                            }
                        }
                        else
                        {
                            if (move.x < -maximumMovePressed)
                            {
                                move.x = -maximumMovePressed;
                            }
                            if (move.y < -maximumMovePressed)
                            {
                                move.y = -maximumMovePressed;
                            }
                            if (move.y > maximumMovePressed)
                            {
                                move.y = maximumMovePressed;
                            }
                            if (move.x > maximumMovePressed)
                            {
                                move.x = maximumMovePressed;
                            }
                        }
                    }
                    /*
                    // Store the last input before the Raw input is 0 so the player keeps moving left or right/up down while in the air and jetpack is altering the direction
                    if (inputHorizontalRaw != 0 && inputVerticalRaw != 0)
                    {
                        move = transform.right * inputHorizontal + transform.up * inputVertical;
                        lastInputVertical = inputVertical;
                        lastInputHorizontal = inputHorizontal;
                    } 
                    else if (inputHorizontalRaw != 0)
                    {
                        move = transform.right * inputHorizontal + transform.up * lastInputVertical;

                        lastInputHorizontal = inputHorizontal;
                    } */


                }
                if (!rotating)
                {
                    // forward speed
                    move.z = speedForward;
                    _controller.Move(velocity * Time.deltaTime + moveSpeed * Time.deltaTime * move);

                }

            }

        }
        else
        { //set pos stuck
            transform.position = startRotPos;
            Debug.Log("pos =" + transform.position + "startpos +" + startRotPos);
            /*//   move.z = speedForward;
            if (currentRot == Rotation.Down)
            {
                velocity.y += gravity * Time.deltaTime;
            }
            else if (currentRot == Rotation.Left)
            {
                velocity.x += gravity * Time.deltaTime;
            }
            else if (currentRot == Rotation.Right)
            {
                velocity.x -= gravity * Time.deltaTime;
            }
            else if (currentRot == Rotation.Up)
            {
                velocity.y -= gravity * Time.deltaTime;
            }
            // Debug.Log("goingZ" + velocity + "move " + move);
            //  _controller.Move(velocity * Time.deltaTime + 1 *Time.deltaTime * move);
            _controller.Move(velocity * Time.deltaTime);
        }*/
        }
    }



    // this part of the momevent script handles the jetpack including animation.
    private void JetpackThrusterParticles()
    {

        if (currentFuel > 0)
        {
            {
                if (inputVerticalRaw < -0.2)
                {
                    fireJetUp.Play();
                }
                else
                {
                    fireJetUp.Stop();
                }
                if (inputVerticalRaw > 0.2)
                {
                    fireJetDownPS.Play();
                }
                else
                {
                    fireJetDownPS.Stop();
                }
                if (inputHorizontalRaw > 0.2)
                {
                    fireJetLowerLeft.Play();
                    fireJetUpperLeft.Play();
                }
                else
                {
                    fireJetLowerLeft.Stop();
                    fireJetUpperLeft.Stop();
                }
                if (inputHorizontalRaw < -0.2)
                {
                    fireJetUpperRight.Play();
                    fireJetLowerRight.Play();
                }
                else
                {
                    fireJetUpperRight.Stop();
                    fireJetLowerRight.Stop();
                }
            }
        }
    }

    // Stop jetpack when touching ground.
    private void StopJetPack()
    {
        fireJetUpperRight.Stop();
        fireJetLowerRight.Stop();
        fireJetDownPS.Stop();
        fireJetUp.Stop();
        fireJetLowerLeft.Stop();
        fireJetUpperLeft.Stop();
    }

    private void JetPackTurn(Quaternion transformRot, Quaternion targetRot)
    {
        float t = 0f;
        float rotateDirectionZ = targetRot.z - transformRot.z;

        var emitParams = new ParticleSystem.EmitParams();
        emitParams.startColor = Color.red;
        if (rotateDirectionZ < 0f) //toleft
        {
            fireJetLowerLeft.Emit(emitParams, 7);
            fireJetLowerRight.Emit(emitParams, 7);

        }
        else if (rotateDirectionZ > 0f) //toright
        {
            fireJetUpperLeft.Emit(emitParams, 7);
            fireJetUpperRight.Emit(emitParams, 7);

        }
        if (!rotating && currentState == PlayerState.Grounded)
        {
            Vector3 moveNormalized = move.normalized; // normalize the movement vector to get the direction of movement
            Vector3 moveDirection = new Vector3(moveNormalized.x, 0, moveNormalized.z); //project the movement vector in xz-plane to y-axis
            Quaternion targetRotation = Quaternion.LookRotation(moveDirection); //get the target rotation based on the movement direction
            legs.transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 4 * Time.deltaTime); // rotate the legs object towards the target rotation over time
        }
    }


    public void AddFuel(float FuelToAdd)
    {

        if (currentFuel < totalFuel)
        {
            Debug.Log("currentHealth" + currentFuel + "tot" + totalFuel);
            if ((FuelToAdd + currentFuel) > totalFuel)
            {
                totalFuel = 100;
            }
            currentFuel += FuelToAdd;
            FuelSlider.value = currentFuel;
        }
    }
    public void RemoveFuel(float FuelToRemove)
    {
        if (currentFuel > 0)
        {
            Debug.Log("currentFuel" + currentFuel + "tot" + totalFuel);
            currentFuel -= FuelToRemove;
            Debug.Log("currentFuel" + currentFuel + "tot" + totalFuel);
            FuelSlider.value = currentFuel;
            if (currentFuel <= 0)
            {
                //  gameOver.SetActive(true);
                //   this.GetComponent<PlayerMovement>().anim.enabled = false;
                //  this.GetComponent<PlayerMovement>().enabled = false;
            }
        }
    }

    private void decreaseFuelVert() {
         horizontalInput = Input.GetAxisRaw("Horizontal");
        // decrease fuel on first press or direction change
        if ((previousHorizontalInput == 0 || previousHorizontalInput != horizontalInput) && Time.time >= nextDecreaseTime)
        {
            RemoveFuel(firstPressDecrease);
            nextDecreaseTime = Time.time + 1f; // wait 1 second before decreasing fuel again
            Debug.Log("1");
        }
        // decrease fuel if it's time
        else if (Time.time >= nextDecreaseTime)
        {
            RemoveFuel(decreaseRate);
            nextDecreaseTime = Time.time + 0.3f; // wait 1 second before decreasing fuel again
            Debug.Log("2");
        }
        previousHorizontalInput = horizontalInput;

    }

    private void decreaseFuelHori()
    {
        
        verticalInput = Input.GetAxisRaw("Horizontal");
        // decrease fuel on first press or direction change
        if ((previousVerticalInput == 0 || previousHorizontalInput != verticalInput) && Time.time >= nextDecreaseTime)
        {
            Debug.Log("3");
            RemoveFuel(firstPressDecrease);
            nextDecreaseTime = Time.time + 1f; // wait 1 second before decreasing fuel again
        }
        // decrease fuel if it's time
        else if (Time.time >= nextDecreaseTime)
        {
            Debug.Log("4");
            RemoveFuel(decreaseRate);
            nextDecreaseTime = Time.time + 0.3f; // wait 1 second before decreasing fuel again
        }
        previousVerticalInput = verticalInput;
    
      
    }

}


// rotate legs or player or 



/*     {
         //move// = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized;

         if (move != Vector3.zero)
         {
             move = Vector3.ProjectOnPlane(move, Vector3.forward);
             Quaternion targetRotation = Quaternion.LookRotation(move, Vector3.forward);
             legs.transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 1 * Time.deltaTime);
         }
     }

     else { legs.transform.rotation = Quaternion.identity; }*/
