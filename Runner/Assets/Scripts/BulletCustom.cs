using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCustom : MonoBehaviour
{

    private Rigidbody rb;
    public GameObject explosion;
    public LayerMask Enemies;

    public float explosionForce;

    //stats
    [Range(0f, 1f)]
    public float bounciness;
    public bool useGravity;

    //Damage
    public int explosionDamage;
    public float explosionRange;

    //lifetime
    public int maxCollisions;
    public float maxLifetime;
    public bool explodeOnTouch = true;

    int collisions;
    PhysicMaterial physics_mat;
    // Start is called before the first frame update

    private void Start()
    {
        Setup();
    }
    //create physics material based off what is inserted in the inspector


    private void Explode()
    {
       
        //Instantiate explosion
        if (explosion != null) StartCoroutine(CreateExplosions());

        //check for enemies for explosion
        Collider[] enemies = Physics.OverlapSphere(transform.position, explosionRange, Enemies);
        for (int i = 0; i < enemies.Length; i++)
        {
            //Get component of enemy and call the script to take Damage

            //TODO CREATE ENEMY SHOOTING AI
            //add explosion force (if enemy has rigidbody)
            if (enemies[i].GetComponent<EnemyAi>())
            {
                enemies[i].GetComponent<EnemyAi>().TakeDamage(explosionDamage);
                //enemies[i].GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, explosionRange);
                //   enemies[i].gameObject.tag = "DeadEnemy";
                //   enemies[i].GetComponent<BoxCollider>().enabled = false;
                //  StartCoroutine(DestroyOther(enemies[i].gameObject));

            }
           else if (enemies[i].GetComponent<Health>())
            {
                enemies[i].GetComponent<Health>().RemoveHealth(explosionDamage);
                //enemies[i].GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, explosionRange);
                //   enemies[i].gameObject.tag = "DeadEnemy";
                //   enemies[i].GetComponent<BoxCollider>().enabled = false;
                //  StartCoroutine(DestroyOther(enemies[i].gameObject));

            }
        }
        if (enemies.Length == 0)
        {
            Invoke("DelayDestroy", 0.00001f);
        }
     Invoke("DelayDestroy", 0.00001f);

    }

    IEnumerator DestroyOther(GameObject other)
    {
       yield return new WaitForSeconds(0.5f);

        Destroy(other);
        Invoke("DelayDestroy", 0.00001f);
       
    }
    private void DelayDestroy()
    {
        Destroy(gameObject);
    }

    IEnumerator CreateExplosions()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Debug.Log("creatingEXP");
        yield return new WaitForSeconds(0.0f);
        //Add a little delay, just to make sure everything works fine
    
        yield return null;
    }
    private void Setup()
    {

        rb = this.GetComponent<Rigidbody>();
        //create physics mat
        physics_mat = new PhysicMaterial();
        physics_mat.bounciness = bounciness;
        physics_mat.frictionCombine = PhysicMaterialCombine.Minimum;
        physics_mat.frictionCombine = PhysicMaterialCombine.Maximum;

        //set gravity

        rb.useGravity = useGravity;

        //assign material to collider
        GetComponent<SphereCollider>().material = physics_mat;
    }
    

    // Update is called once per frame
    void Update()
    {
        //when explode
        if (collisions > maxCollisions) Explode();

        // count timer till explosing
        maxLifetime -= Time.deltaTime;
        if (maxLifetime <= 0) Explode();

    }

    private void OnCollisionEnter(Collision collision)
    {
        collisions++;
        Debug.Log("colliding with??" + collision.gameObject);
        //if bullet hits  an enemy and explodeOnTouch is activated
        if (collision.collider.CompareTag("Enemy") && explodeOnTouch) Explode();

    }

    private void OnTriggerEnter(Collider collision)
    {
        collisions++;
        Debug.Log("colliding with??" + collision.gameObject);
        //if bullet hits  an enemy and explodeOnTouch is activated
        if (collision.tag == ("Enemy") && explodeOnTouch) Explode();

    }
}
