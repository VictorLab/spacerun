using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractible
{
    // This will be call when this interactible is picked up
    void OnPickUp();
    // This will be call when this interactible is used
    void OnUse();
    // This will be call when this interactible is dropped
    void OnDrop();
}