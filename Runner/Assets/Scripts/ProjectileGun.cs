using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ProjectileGun : MonoBehaviour
{
    //bullet
    [SerializeField]
    private GameObject bullet;

    private AudioSource audio;
    //bullet force
    [SerializeField]
    private float shootForce, upwardForce;

    //gun stats
    [SerializeField]
    private float timeBetweenShooting, spread, reloadTime, timeBetweenShots;

    [SerializeField]
    private int magazineSize, bulletsPerTap;

    [SerializeField]
    private bool allowButtonHold;
  
    private int bulletsLeft, bulletsShot;

    //bools
    private bool shooting, readyToShoot, reloading;


    //Reference
    [SerializeField]
    private GameObject player;
    
    [SerializeField]
    private Transform attackPoint;

    //UI
    [SerializeField]
    private TextMeshProUGUI ammunitionDisplay;
    //second proj
    [SerializeField]
    private GameObject secondProjectile;
    //bug fixing 
    public bool allowInvoke = true;

    // Start is called before the first frame update
    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
        //bullet = secondProjectile;
        audio = player.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        MyInput();
     //   Debug.DrawRay(player.transform.position, transform.forward, Color.green);

    }
    private void MyInput()
    {
        // Check if allowed to hold down the shoot button or only fire when pressing.
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        //Shooting
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            //set bullets shot to 0
            bulletsShot = 0;

            Shoot();
        }

     
        //reloading on R
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading) Reload();
        //Reload when trying to shoot without ammo
        if (readyToShoot && shooting && !reloading && bulletsLeft <= 0) Reload();

    }

    //decrease ammo 
    private void Shoot()
    {
        readyToShoot = false;

        //Find middle of the screen where the  bullet should go using a raycast (possibly change to mouse later)
        // Ray ray = Cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Ray ray = new Ray(attackPoint.transform.position, player.transform.forward);
        RaycastHit hit;

        //Check if ray hits something
        Vector3 targetPoint;
       
        if (Physics.Raycast(ray, out hit))
        {
            targetPoint = hit.point;
            Debug.Log("rayhit1 " + hit.transform.gameObject);
        }
        else
        {
            targetPoint = ray.GetPoint(99); // Point away from the player when ray does not hit
            Debug.Log("rayhit2 " + ray.GetPoint(99));
        }
        //Calculate direction from attackpoint to targetpoint
        Vector3 directionWithoutSpread = targetPoint - attackPoint.position;

        //Calculate spread
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);
        
        //calculate new direction with spread
        Vector3 directionWithSpread = directionWithoutSpread + new Vector3(x, y, 0);

        //instantiate bullet
        GameObject currentBullet = Instantiate(bullet, attackPoint.position, Quaternion.identity);
        Debug.Log("createbullet + " + attackPoint.position);
        //roate bullet to shoot direction
        currentBullet.transform.forward = directionWithSpread.normalized;

        //add forces to bullet
        currentBullet.GetComponent<Rigidbody>().AddForce(directionWithSpread.normalized * shootForce, ForceMode.Impulse);

        audio.Play();
        bulletsLeft--;
        bulletsShot++;
        ChangeAmmoDisplay();
        //Invoke resetShot function for waiting duration timeBetweenShooting
        if (allowInvoke)
        {
            Invoke("ResetShot", timeBetweenShooting);
            allowInvoke = false;
        }

        //if more than one bulletsPerTrap make sure to repeat shoot function
        if (bulletsShot < bulletsPerTap && bulletsLeft > 0)
            Invoke("Shoot", timeBetweenShots);
    }

    private void ResetShot()
    {
        // Allow shooting and invoking again
        readyToShoot = true;
        allowInvoke = true;
    }

    private void Reload()
    {
        reloading = true;
        Invoke("ReloadFinished", reloadTime);
    }


    public void AddAmmo(int ammoPlus)
    {
        if (ammoPlus + bulletsLeft < magazineSize)
        {
            bulletsLeft += ammoPlus;
            ChangeAmmoDisplay();
        }
        else
        {
            bulletsLeft = magazineSize;
        }
       
        Debug.Log("addamo");
    }

    //reload
    private void ReloadFinished()
    {
        bulletsLeft = magazineSize;
        reloading = false;
        ChangeAmmoDisplay();
    }

    private void ChangeAmmoDisplay()
    {
        //Set ammo display, if it exists 
        if (ammunitionDisplay != null)
            ammunitionDisplay.SetText("Ammo:" + bulletsLeft / bulletsPerTap + " / " + magazineSize / bulletsPerTap);
    }
}


//todo: object pooling





// if grenade etc: currentBullet.GetComponent<Rigidbody>().AddForce(player * shootForce, ForceMode.Impulse);