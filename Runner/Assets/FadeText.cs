using UnityEngine;
using TMPro;

public class FadeText : MonoBehaviour
{
    public float fadeSpeed = 0.5f;
    private TextMeshProUGUI text;

    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        Color textColor = text.color;
        textColor.a -= fadeSpeed * Time.deltaTime;
        text.color = textColor;
    }
}