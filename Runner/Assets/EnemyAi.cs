
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyAi : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float visibilityThreshold = 30f;  // The distance beyond which the health bar will be hidden
    [SerializeField]
    private float health;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject projectile;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public bool moveTowards;

    [SerializeField]
    private Transform shootFrom;

    
  public GameObject[] objects = new GameObject[10];

    //healthbar
    [SerializeField]
    private Slider HealthSlider;

    private Vector3 halfExtendsSight;
    private Vector3 halfExtendsAttack;
    private void Awake()
    {
        HealthSlider.gameObject.SetActive(false);

        // Only fetch the player's reference once.
        player = GameObject.FindGameObjectWithTag("Player").transform;

        agent = GetComponent<NavMeshAgent>();

        halfExtendsSight = new Vector3(sightRange, 0.01f, sightRange);
        halfExtendsAttack = new Vector3(attackRange, 0.01f, attackRange);
    
        if (player == null)
        {
            Debug.Log("player still null!!");
            player = GameObject.FindWithTag("Player").transform;
        }
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckBox(transform.position, halfExtendsSight, transform.rotation, whatIsPlayer);
        playerInAttackRange = Physics.CheckBox(transform.position, halfExtendsAttack, transform.rotation, whatIsPlayer);

        // Debug.Log("playerinsight" + playerInSightRange + "playerInAttackRange" + playerInAttackRange);
        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (playerInAttackRange && playerInSightRange) AttackPlayer();


        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        // Debugging
       

        // Only enable the health bar when the enemy is within the visibility threshold.
        if (distanceToPlayer <= visibilityThreshold)
        {
            HealthSlider.gameObject.SetActive(true);
            Debug.Log(gameObject.name + " - Health Bar Activated");
            Debug.Log(gameObject.name + " - Distance to Player: " + distanceToPlayer);
        }
        else
        {
            HealthSlider.gameObject.SetActive(false);
            Debug.Log(gameObject.name + " - Health Bar Deactivated");
            Debug.Log(gameObject.name + " - Distance to Player: " + distanceToPlayer);
        }


    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet && agent.isOnNavMesh)
        {
            agent.SetDestination(walkPoint);
        }
        else if (!agent.isOnNavMesh)
        {
            Debug.LogWarning("Agent '" + gameObject.name + "' is not on a valid NavMesh when trying to set walkPoint!");
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        // Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }
    private void ChasePlayer()
    {
        if (moveTowards)
        {
            if (agent.isOnNavMesh)
            {
                agent.SetDestination(player.position);
            }
            else
            {
                Debug.LogWarning("Agent '" + gameObject.name + "' is not on a valid NavMesh!");
            }
            agent.speed = 11;
        }
        else
        {
            agent.speed = 4;
        }
    }


    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        transform.LookAt(player);
        if (!moveTowards)
        {
            //transform.LookAt(player);

            if (!alreadyAttacked && !moveTowards)
            {
                ///Attack code here
                Rigidbody rb = Instantiate(projectile, shootFrom.position, Quaternion.identity).GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * 500f, ForceMode.Impulse);
                rb.AddForce(transform.right * -20f, ForceMode.Impulse);
                rb.AddForce(transform.up * -20f, ForceMode.Impulse);
                ///End of attack code

                alreadyAttacked = true;
                Invoke(nameof(ResetAttack), timeBetweenAttacks);
                //      }
            }
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        HealthSlider.value = health;
        if (health <= 0) Invoke(nameof(DestroyEnemy), 0.05f);
       
    }
    private void DestroyEnemy()
    {
        Instantiate(objects[Random.RandomRange(0, 6)], transform.position, Quaternion.identity).GetComponent<Rigidbody>();
        Destroy(gameObject);
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
