using TMPro;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;


[ExecuteInEditMode, RequireComponent(typeof(Camera))]
public class CartoonPostProcessingEffect : MonoBehaviour
{
    public Material cartoonMaterial;
    public Texture2D nebulaTexture;
    public TextMeshProUGUI effectText;
    private List<string> effects;
    private List<string> activeEffects = new List<string>();
    private List<string> lightEffects = new List<string>();
    private List<string> mediumEffects = new List<string> { "CannyEdge", "ScanLine", "Glitch", "MotionBlur" };
    private string hardEffect = "NebulaOverlay";
    private int currentEffectIndex = 0;
    private float fadeStartTime;
    private string currentEffect = "";  // Reintegrated variable
    private int appliedEffectCount = 0; // Count of currently applied effects
    private int applyEffectCalls = 0; // Number of times ApplyRandomCombinationEffects has been called
    // List to keep track of the remaining effects to be applied.
    List<string> remainingEffects = new List<string>();
    Dictionary<string, int> effectApplicationCounts = new Dictionary<string, int>();
    // Flags to keep track of the NebulaOverlay, EdgeDetection, and CannyEdge application.
    bool nebulaApplied = false;
    bool edgeDetectionApplied = false;

    private void Start()
    {

        effects = new List<string>
        {
            "Pixelation",
            "ChromaticAberration",
            "Vignette",
            "Bloom",
            "Noise",
            "Fog",
            "Desaturation",
            "Darkening",
            "Starfield",
            "WindLines",
            "ScanLine",
            "EdgeDetection",
            "Bloody",
            "Glitch",
            "Screenshake",
            "CannyEdge",
            "MotionBlur",
            "Ghosting",
            "DynamicFOV",
            "NebulaOverlay"
        };
        foreach (string effect in effects)
        {
            effectApplicationCounts[effect] = 0; // Initialize all counts to 0
        }


        Camera.main.fieldOfView = 50;

        RenderSettings.fog = true;
        RenderSettings.fogColor = Color.black;
        RenderSettings.fogMode = FogMode.Exponential;
        RenderSettings.fogDensity = 0.05f;

        lightEffects = effects.Except(mediumEffects).ToList();
        lightEffects.Remove(hardEffect);

        cartoonMaterial.SetTexture("_NebulaOverlay", nebulaTexture);
       

        // Listen to player damage event
        // Check for "R" key press
      
    }

    private Dictionary<string, string> effectToShaderPropertyMap = new Dictionary<string, string>
{
    {"Pixelation", "_PixelationAmount"},
    {"ChromaticAberration", "_ChromaticAberration"},
    {"Vignette", "_VignetteIntensity"},
    {"Bloom", "_BloomIntensity"},
    {"Noise", "_NoiseIntensity"},
    {"Fog", "_FogIntensity"},
    {"Desaturation", "_DesaturationAmount"},
    {"Darkening", "_DarkeningAmount"},
    {"Starfield", "_StarfieldIntensity"},
    {"WindLines", "_WindLinesIntensity"},
    {"ScanLine", "_ScanLineFrequency"},
    {"EdgeDetection", "_EdgeDetectionIntensity"},
    {"CannyEdge", "_EdgeDetectionIntensity"},
    {"ScreenShake", "_ShakeIntensity"},
    {"Bloody", "_BloodyIntensity"},
    {"Glitch", "_GlitchIntensity"},
    {"MotionBlur", "_MotionBlurIntensity"},
    {"Ghosting", "_GhostingIntensity"},
    {"DynamicFOV", "_DynamicFOVFactor"},
    {"NebulaOverlay", "_NebulaOverlayIntensity"}
};








    private void Update()
    {
        /*
        // R for different combo of effects
        if (Input.GetKeyDown(KeyCode.R))
        {
            ApplyRandomCombinationEffects();
        }
        // Q to remove the text displaying effects
        if (Input.GetKeyDown(KeyCode.Q))
        {
            effectText.gameObject.SetActive(!effectText.gameObject.activeSelf);
        }
        // E for cycling forward through effects
        if (Input.GetKeyDown(KeyCode.E))
        {
            CycleEffectForward();
        }
        // F for cycling backward through effects
        if (Input.GetKeyDown(KeyCode.F))
        {
            CycleEffectBackward();
        }
        // G for applying a random combination of effects
        if (Input.GetKeyDown(KeyCode.G))
        {
            ApplyRandomCombinationEffects();
        }
        // H for removing the last added shader effect from the active list
        if (Input.GetKeyDown(KeyCode.H))
        {
            TriggerRandomEffect();
        }
        // J for reversing the order of the current shaders
        if (Input.GetKeyDown(KeyCode.J))
        {
            ReverseCurrentShadersOrder();
        }*/
        // Fade-in logic
        // Fade-in logic
        if (!string.IsNullOrEmpty(currentEffect))
        {
            float elapsed = Time.time - fadeStartTime;
            if (elapsed < 1.5f)
            {
                float t = elapsed / 1.5f;

                // Use the mapping to get the correct shader property name
                string shaderProperty = effectToShaderPropertyMap[currentEffect];

                float currentValue = cartoonMaterial.GetFloat(shaderProperty);
                cartoonMaterial.SetFloat(shaderProperty, Mathf.Lerp(0, currentValue, t));
            }
            else
            {
                currentEffect = "";
            }
        }

    }

    private void TriggerRandomEffect()
    {
        if (lightEffects.Count > 0)
        {
            int index = Random.Range(0, lightEffects.Count);
            currentEffect = lightEffects[index];
            lightEffects.RemoveAt(index);
            ApplyEffect(currentEffect);
        }
        else if (mediumEffects.Count > 0)
        {
            int index = Random.Range(0, mediumEffects.Count);
            currentEffect = mediumEffects[index];
            mediumEffects.RemoveAt(index);
            ApplyEffect(currentEffect);
        }
        else
        {
            currentEffect = hardEffect;
            ApplyEffect(currentEffect);
        }

        fadeStartTime = Time.time;
    }
    // Method to cycle forward through effects
    void CycleEffectForward()
    {
        ResetAllEffects();
        currentEffectIndex = (currentEffectIndex + 1) % effects.Count;
        ApplyEffect(effects[currentEffectIndex]);
        effectText.text = effects[currentEffectIndex];
    }

    // Method to cycle backward through effects
    void CycleEffectBackward()
    {
        ResetAllEffects();
        currentEffectIndex--;
        if (currentEffectIndex < 0)
        {
            currentEffectIndex = effects.Count - 1;
        }
        ApplyEffect(effects[currentEffectIndex]);
        effectText.text = effects[currentEffectIndex];
    }

    // Method to add the current effect to the active list
    void AddEffectToActiveList()
    {
        // Check if the effect is already in the active list
        if (!activeEffects.Contains(effects[currentEffectIndex]))
        {
            activeEffects.Add(effects[currentEffectIndex]);
        }
        // Apply all effects in the active list
        foreach (string effect in activeEffects)
        {
            ApplyEffect(effect);
        }
    }

  

    // Method to remove the last added effect from the active list
    void RemoveLastEffectFromActiveList()
    {
        if (activeEffects.Count > 0)
        {
            activeEffects.RemoveAt(activeEffects.Count - 1);
            // Reapply the remaining effects in the active list
            ResetAllEffects();
            foreach (string effect in activeEffects)
            {
                ApplyEffect(effect);
            }
        }
    }

    // Method to reverse the order of the current shaders
    void ReverseCurrentShadersOrder()
    {
        effects.Reverse();
        // Adjust the current effect index after reversing the list
        currentEffectIndex = effects.Count - 1 - currentEffectIndex;
    }

    void ToggleTextVisibilityOnClick()
    {
        // Create a ray from the mouse cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Check if the ray hits an object
        if (Physics.Raycast(ray, out hit))
        {
            // If the hit object is the TextMeshProUGUI, toggle its active state
            if (hit.collider.gameObject == effectText.gameObject)
            {
                effectText.gameObject.SetActive(!effectText.gameObject.activeSelf);
            }
        }
    }

    // Coroutine to iterate through the effects.
    IEnumerator ChangeEffect()
    {
        // Loop through individual effects.
        for (int i = 0; i < effects.Count; i++)
        {
            // Show individual effect.
            ResetAllEffects();
            ApplyEffect(effects[i]);
            effectText.text = effects[i];
            yield return new WaitForSeconds(2);
        }

        // Show random combination of effects.
        while (true)
        {
            ApplyRandomCombinationEffects();
            yield return new WaitForSeconds(3);
        }
    }
    public void ApplyRandomCombinationEffects()
    {
        applyEffectCalls++;
        Debug.Log("Apply Effect Calls: " + applyEffectCalls);

        // After CannyEdge is applied, reset everything.
        if (nebulaApplied && edgeDetectionApplied && applyEffectCalls > 9) // Tripled the waiting time
        {
            ResetAllEffects();
            appliedEffectCount = 0;
            remainingEffects.Clear();
            nebulaApplied = false;
            edgeDetectionApplied = false;
            applyEffectCalls = 0;
            effectText.text = ""; // Clearing the effect text as well.
            return; // Exit the method after resetting.
        }

        if (appliedEffectCount + 2 > 4)
        {
            Debug.Log("Resetting effects");
            ResetAllEffects();
            appliedEffectCount = 0;
            remainingEffects.Clear();
            nebulaApplied = false;
            edgeDetectionApplied = false;
            applyEffectCalls = 0;
        }

        if (applyEffectCalls > 9)  // Tripled the waiting time from 3 to 9
        {
            if (!nebulaApplied)
            {
                ApplyEffect("NebulaOverlay");
                nebulaApplied = true;
                effectText.text += "NebulaOverlay, ";
            }
            else if (!edgeDetectionApplied)
            {
                ApplyEffect("EdgeDetection");
                edgeDetectionApplied = true;
                effectText.text += "EdgeDetection, ";
            }
            else
            {
                ApplyEffect("CannyEdge");
                effectText.text += "CannyEdge";
            }

            return;
        }

        if (remainingEffects.Count == 0)
        {
            remainingEffects = new List<string>(effects);
            remainingEffects.Remove("NebulaOverlay");
            remainingEffects.Remove("CannyEdge");
            remainingEffects.Remove("EdgeDetection");
        }

        for (int i = 0; i < 2; i++)
        {
            if (remainingEffects.Count > 0)
            {
                int randomIndex = Random.Range(0, remainingEffects.Count);
                string effectToApply = remainingEffects[randomIndex];
                ApplyEffect(effectToApply);
                remainingEffects.RemoveAt(randomIndex);
                Debug.Log("Applied effect: " + effectToApply);
                appliedEffectCount++;
                Debug.Log("Total applied effects: " + appliedEffectCount);
                effectText.text += effectToApply + ", ";
            }
        }
    }




    // Function to reset all effects to their default values.
    void ResetAllEffects()
    {
        // Set all effect properties to their "off" values.
        cartoonMaterial.SetFloat("_PixelationAmount", 0);
        cartoonMaterial.SetFloat("_ChromaticAberration", 0);
        cartoonMaterial.SetFloat("_VignetteIntensity", 0);
        cartoonMaterial.SetFloat("_BloomIntensity", 0);
        cartoonMaterial.SetFloat("_NoiseIntensity", 0);
        cartoonMaterial.SetFloat("_FogIntensity", 0);
        cartoonMaterial.SetFloat("_DesaturationAmount", 0);
        cartoonMaterial.SetFloat("_DarkeningAmount", 0);
        cartoonMaterial.SetFloat("_StarfieldIntensity", 0);
        cartoonMaterial.SetFloat("_WindLinesIntensity", 0);
        cartoonMaterial.SetFloat("_ScanLineFrequency", 0);
        cartoonMaterial.SetFloat("_EdgeDetectionIntensity", 0);
        cartoonMaterial.SetFloat("_ShakeIntensity", 0);
        cartoonMaterial.SetFloat("_BloodyIntensity", 0);
        cartoonMaterial.SetFloat("_GlitchIntensity", 0);
        cartoonMaterial.SetFloat("_MotionBlurIntensity", 0);
        cartoonMaterial.SetFloat("_GhostingIntensity", 0);
        cartoonMaterial.SetFloat("_DynamicFOVFactor", 0);
        // Assuming NebulaOverlay has an intensity property
        cartoonMaterial.SetFloat("_NebulaOverlayIntensity", 0);
    }

    // Function to apply a specific effect based on its name.
    public void ApplyEffect(string effectName, float intensity = 1.0f)
    {
        // Ensure intensity is within the range [0.33, 1.0]
        intensity = Mathf.Clamp(intensity, 0.33f, 1.0f);

        switch (effectName)
        {
            case "Pixelation":
                cartoonMaterial.SetFloat("_PixelationAmount", 2 * intensity);
                break;
            case "ChromaticAberration":
                cartoonMaterial.SetFloat("_ChromaticAberration", 0.1f * intensity);
                break;
            case "Vignette":
                cartoonMaterial.SetFloat("_VignetteIntensity", 5f * intensity);
                break;
            case "Bloom":
                cartoonMaterial.SetFloat("_BloomIntensity", 4f * intensity);
                break;
            case "Noise":
                cartoonMaterial.SetFloat("_NoiseIntensity", 2f * intensity);
                break;
            case "Fog":
                cartoonMaterial.SetFloat("_FogIntensity", 4f * intensity);
                break;
            case "Desaturation":
                cartoonMaterial.SetFloat("_DesaturationAmount", 4f * intensity);
                break;
            case "Darkening":
                cartoonMaterial.SetFloat("_DarkeningAmount", 0.5f * intensity);
                break;
            case "Starfield":
                cartoonMaterial.SetFloat("_StarfieldIntensity", 10f * intensity);
                break;
            case "WindLines":
                cartoonMaterial.SetFloat("_WindLinesIntensity", 2f * intensity);
                break;
            case "ScanLine":
                cartoonMaterial.SetFloat("_ScanLineFrequency", 5 * intensity);
                break;
            case "EdgeDetection":
                cartoonMaterial.SetFloat("_EdgeDetectionIntensity", 0.2f * intensity);
                break;
            case "CannyEdge":
                cartoonMaterial.SetFloat("_EdgeDetectionIntensity", 1.1f * intensity);
                break;
            case "ScreenShake":
                cartoonMaterial.SetFloat("_ShakeIntensity", 1.0f * intensity);
                break;
            case "Bloody":
                cartoonMaterial.SetFloat("_BloodyIntensity", 1f * intensity);
                cartoonMaterial.SetVector("_BloodyUVOffset", new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
                cartoonMaterial.SetFloat("_BloodyFadeAmount", 1f * intensity);
                StartCoroutine(FadeOutBloodEffect(5f * intensity));  // 5 seconds fade duration, adjust as needed
                break;
            case "Glitch":
                cartoonMaterial.SetFloat("_GlitchIntensity", 1.5f * intensity);
                break;
            case "MotionBlur":
                cartoonMaterial.SetFloat("_MotionBlurIntensity", 0.8f * intensity);
                break;
            case "Ghosting":
                cartoonMaterial.SetFloat("_GhostingIntensity", 0.7f * intensity);
                break;
            case "DynamicFOV":
                cartoonMaterial.SetFloat("_DynamicFOVFactor", 0.0f); // DynamicFOV is off by default, so no need for intensity scaling.
                break;
            case "NebulaOverlay":
                cartoonMaterial.SetFloat("_NebulaOverlayIntensity", 0.5f * intensity);
                break;
            default:
                break;
        }
    }


    IEnumerator FadeOutBloodEffect(float fadeDuration)
    {
        float elapsed = 0;
        while (elapsed < fadeDuration)
        {
            float fadeAmount = 1 - (elapsed / fadeDuration);
            cartoonMaterial.SetFloat("_BloodyFadeAmount", fadeAmount);
            elapsed += Time.deltaTime;
            yield return null;
        }
        cartoonMaterial.SetFloat("_BloodyFadeAmount", 0);
    }

    public void ApplyDamageEffects()
    {
        // Apply damage effects without resetting the current effects
        ApplyEffect("Bloody", Random.Range(0.5f, 1.0f));
        ApplyEffect("MotionBlur", Random.Range(0.1f, 0.7f));
        ApplyEffect("Glitch", Random.Range(0.1f, 0.7f));
    }

    // This function generates combinations.
    public static IEnumerable<IEnumerable<T>> GetCombinations<T>(IEnumerable<T> list, int length)
    {
        if (length == 1) return list.Select(t => new T[] { t });
        return GetCombinations(list, length - 1)
            .SelectMany(t => list.Where(o => !t.Contains(o)),
                (t1, t2) => t1.Concat(new T[] { t2 }));
    }

  

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        // Apply the cartoonMaterial if it's not null.
        if (cartoonMaterial != null)
        {
            Graphics.Blit(src, dest, cartoonMaterial);
        }
        else
        {
            Graphics.Blit(src, dest);
        }
    }
    private void OnDisable()
    {
        Health.OnPlayerDamaged -= ApplyDamageEffects;

    }
}
public class FlickeringLight : MonoBehaviour
{
    public Light lightToAffect;
    public float minWaitTime = 0.1f;
    public float maxWaitTime = 0.5f;

    private void Start()
    {
        StartCoroutine(Flicker());
    }

    IEnumerator Flicker()
    {
        while (true)
        {
            lightToAffect.enabled = !lightToAffect.enabled;
            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
        }
    }
}